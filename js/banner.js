// wl
$(function () {
  hide_swiper();
    var mySwiper = new Swiper ('.swiper-container', {
      // Optional parameters
      direction: 'horizontal',
      loop: true,
      speed:300,
     
  
      // If we need pagination
      pagination: {
        el: '.swiper-pagination',
        clickable:true,
        
      },
  
      // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
  
      // And if we need scrollbar
      scrollbar: {
        el: '.swiper-scrollbar',
      },
    })
   
    //相当于window.onload
});

function hide_swiper(){
  //1.初始状态隐藏此部分
  $(".swiper-button-prev").hide();
  $(".swiper-button-next").hide();
  //2.当鼠标滚动到某一值得时候，显示此部分内容
  $(".swiper-container").mouseover(function(){
    $(".swiper-button-prev").show(200);
    $(".swiper-button-next").show(200);
    $(".swiper-container").mouseleave(function () { 
      $(".swiper-button-prev").hide(500);
  $(".swiper-button-next").hide(500);
      
    });
    
  });
  

}
 